mod console;

use console::Console;
use colored::*;
use rprompt::prompt_reply_stdout;


fn main() {
    let is_chroot = false;

    println!("{} Gentoo Installer - {}", hacks("cc", ""), "v0.1-alpha".red());
    Console::warn("Not Ready For A Gentoo Install");
    
    loop {
        mainmenu(is_chroot);
    }
}

fn mainmenu(chroot: bool) {
    if chroot == false {
        println!("{} Prepare Disks", hacks("num", "1"));
        println!("{} Mount Partitions", hacks("num", "2"));
        println!("{} Install Base System", hacks("num", "3"));
        println!("{} Chroot", hacks("num", "4"));
        println!("{} Exit", hacks("num", "5"));
    } else {
        println!("{} Sync", hacks("num", "1"));
        println!("{} Update World Set", hacks("num", "2"));
        println!("{} Set Essentials", hacks("num", "3"));
        println!("{} Kernel Configuration (go to bash)", hacks("num", "4"));
        println!("{} Generate Fstab", hacks("num", "5"));
        println!("{} Configure Network", hacks("num", "6"));
        println!("{} Boot Loader", hacks("num", "7"));
        println!("{} Exit", hacks("num", "8"));
    }

    print!("{}", hacks("p", ""));
    let s = prompt_reply_stdout(" ").unwrap();
    let opt = s.parse::<i32>().unwrap();

    if chroot == false {
        match opt {
            1 => println!(""),
            2 => println!(""),
            3 => println!(""),
            4 => println!(""),
            5 => std::process::exit(0),
            _ => println!("Invalid Input")
        }
    }
}

fn hacks(n: &str,i: &str) -> ColoredString {
    let cc = ":: ".green();
    let p = "> ".green();
    let num = (i.to_owned() + ". ").green();
    
    if n == "cc" { return cc; };
    if n == "p" { return p; };
    if n == "num" && i != "" { return num; };
    return ColoredString::default();
}