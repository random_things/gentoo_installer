use colored::*;

pub struct Console;

impl Console {
    #[allow(dead_code)]
    pub fn log(msg: &str) {
        println!("{} {}", "[INFO]".blue(), msg.blue());
    }

    #[allow(dead_code)]
    pub fn warn(msg: &str) {
        println!("{} {}", "[WARNING]".yellow(), msg.yellow())
    }
    
    #[allow(dead_code)]
    pub fn err(msg: &str) {
        println!("{} {}", "[ERROR]".red(), msg.red())
    }
}